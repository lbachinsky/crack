#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main(int argc, char *argv[])
{
    // Check number of cmd line args
    if (argc < 3)
    {
        printf("Must supply a source and destination filename\n");
        exit(1);
    }
    
    // Opens first file for reading
    FILE *src = fopen(argv[1], "r");
    if (!src)
    {
        printf("can't open %s for reading\n", argv[1]);
        exit(1);
    }

    
    
    // Open second file for writing
    FILE *dest = fopen(argv[2], "w");
    if (!dest)
    {
        printf("Can't open %s for writing.\n", argv[2]);
        exit(1);
    }
    
    // Loop through first file, writing data to second file as we go
    char line[100];
    int ccount = 1;
    while (fgets(line, 100, src) != NULL)
    {
        line[strlen(line)-1] = '\0';
        char *newline = md5(line, strlen(line));
        
        fprintf(dest, "%d: %s\n", ccount, newline);
        ccount++;
    }
    
    fclose(src);
    fclose(dest);
}